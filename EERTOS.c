#include <EERTOS.h>

// ������� �����, ��������.
// ��� ������ - ��������� �� �������
volatile static TPTR TaskQueue[TaskQueueSize + 1];		// ������� ����������
volatile static struct {
	TPTR GoToTask; 						// ��������� ��������
	int rtc_mode :1;
	u16 delay;							// �������� � ��
	unsigned char hour;
	unsigned char min;
	unsigned char sec;
} MainTimer[MainTimerQueueSize + 1];	// ������� ��������

volatile unsigned char task_schedule_cnt;

// RTOS ����������. ������� ��������
inline void InitRTOS(void) {
	u08 index;
	uint8_t status;
	uint8_t buf[8];

	TWI_SetFreq(50000);
	// Enable SQW/OUT
	TWI_SendDataToRegister(DS1307_ADR, 7, ((1 << DS1307_SQWE)));

	// Read seconds
	buf[0] = DS1307_ADR;
	buf[1] = 0;
	status = TWI_SendData(buf, 2);

	if (status == TWI_SUCCESS) {
		// Clear CH Bit to start RTC
		TWI_ReadData(buf, 8);
		buf[0] &= ~(1 << DS1307_CH);
		TWI_SendDataToRegister(DS1307_ADR, 0, buf[0]);
	}

	for (index = 0; index != TaskQueueSize + 1; index++)// �� ��� ������� ���������� Idle
			{
		TaskQueue[index] = Idle;
	}

	for (index = 0; index != MainTimerQueueSize + 1; index++) // �������� ��� �������.
			{
		MainTimer[index].GoToTask = Idle;
		MainTimer[index].rtc_mode = 0;
		MainTimer[index].delay = 0;
		MainTimer[index].hour = 0;
		MainTimer[index].min = 0;
		MainTimer[index].sec = 0;
	}
}

// Idle Task.
inline void Idle(void) {

}

void SetTask(TPTR TS) {

	u08 index = 0;
	u08 nointerrupted = 0;

	if (STATUS_REG & (1 << Interrupt_Flag)) // ���� ���������� ���������, �� ��������� ��.
			{
		Disable_Interrupt
		nointerrupted = 1;			// � ������ ����, ��� �� �� � ����������. 
	}

	while (TaskQueue[index] != Idle) // ����������� ������� ����� �� ������� ��������� ������
	{									// � ��������� Idle - ����� �������.
		index++;
		if (index == TaskQueueSize + 1) // ���� ������� ����������� �� ������� �� ������ ��������
				{
			if (nointerrupted)
				Enable_Interrupt
			// ���� �� �� � ����������, �� ��������� ����������
			return;// ������ ������� ���������� ��� ������ - ������� �����������. ���� �����.
		}
	}
	// ���� ����� ��������� �����, ��
	TaskQueue[index] = TS;						// ���������� � ������� ������
	if (nointerrupted)
		Enable_Interrupt
	// � �������� ���������� ���� �� � ����������� ����������.
}

//������� ��������� ������ �� �������. ������������ ��������� - ��������� �� �������, 
// ����� �������� � ����� ���������� �������. ��������� ��� ������.
// Days not supported
void SetTimerTaskDelay(TPTR TS, u16 NewTime) {
	u08 index = 0;
	u08 nointerrupted = 0;

	if (STATUS_REG & (1 << Interrupt_Flag)) // �������� ������� ����������, ���������� ������� ����
			{
		Disable_Interrupt
		nointerrupted = 1;
	}

	for (index = 0; index != MainTimerQueueSize + 1; ++index) //����������� ������� ��������
			{
		if (MainTimer[index].GoToTask == TS) // ���� ��� ���� ������ � ����� �������
				{
			MainTimer[index].delay = NewTime;
			MainTimer[index].rtc_mode = 0;
			if (nointerrupted)
				Enable_Interrupt
			// ��������� ���������� ���� �� ���� ���������.
			return;// �������. ������ ��� ��� �������� ��������. ���� �����
		}
	}

	for (index = 0; index != MainTimerQueueSize + 1; ++index) // ���� �� ������� ������� ������, �� ���� ����� ������
			{
		if (MainTimer[index].GoToTask == Idle) {
			MainTimer[index].GoToTask = TS;	// ��������� ���� �������� ������
			MainTimer[index].delay = NewTime;
			MainTimer[index].rtc_mode = 0;
			if (nointerrupted)
				Enable_Interrupt
			// ��������� ����������
			return;// �����. 
		}

	}		// ��� ����� ������� return c ����� ������ - ��� ��������� ��������

}

//������� ��������� ������ �� �������. ������������ ��������� - ��������� �� �������, 
// ����� �������� � ����� ���������� �������. ��������� ��� ������.
void SetTimerTask(TPTR TS, unsigned char hour, unsigned char min,
		unsigned char sec) {
	u08 index = 0;
	u08 nointerrupted = 0;

	if (STATUS_REG & (1 << Interrupt_Flag)) // �������� ������� ����������, ���������� ������� ����
			{
		Disable_Interrupt
		nointerrupted = 1;
	}

	for (index = 0; index != MainTimerQueueSize + 1; ++index) //����������� ������� ��������
			{
		if (MainTimer[index].GoToTask == TS) // ���� ��� ���� ������ � ����� �������
				{
			MainTimer[index].delay = 0;
			MainTimer[index].rtc_mode = 1;
			MainTimer[index].hour = hour;
			MainTimer[index].min = min;
			MainTimer[index].sec = sec;
			if (nointerrupted)
				Enable_Interrupt
			// ��������� ���������� ���� �� ���� ���������.
			return;// �������. ������ ��� ��� �������� ��������. ���� �����
		}
	}

	for (index = 0; index != MainTimerQueueSize + 1; ++index) // ���� �� ������� ������� ������, �� ���� ����� ������
			{
		if (MainTimer[index].GoToTask == Idle) {
			MainTimer[index].GoToTask = TS;	// ��������� ���� �������� ������
			MainTimer[index].delay = 0;			// �������������� �� ��������
			MainTimer[index].rtc_mode = 1;
			MainTimer[index].hour = hour;
			MainTimer[index].min = min;
			MainTimer[index].sec = sec;
			if (nointerrupted)
				Enable_Interrupt
			// ��������� ����������
			return;// �����. 
		}

	}		// ��� ����� ������� return c ����� ������ - ��� ��������� ��������
}

/*=================================================================================
 ��������� ����� ��. �������� �� ������� ������ � ���������� �� ����������.
 */
inline void TaskManager(void) {
	u08 index = 0;
	TPTR GoToTask = Idle;		// �������������� ����������

	Disable_Interrupt
	// ��������� ����������!!!
	GoToTask = TaskQueue[0];		// ������� ������ �������� �� �������

	if (GoToTask == Idle) 			// ���� ��� �����
			{
		Enable_Interrupt
		// ��������� ����������
		(Idle)(); 					// ��������� �� ��������� ������� �����
	} else {
		for (index = 0; index != TaskQueueSize; index++) // � ��������� ������ �������� ��� �������
				{
			TaskQueue[index] = TaskQueue[index + 1];
		}

		TaskQueue[TaskQueueSize] = Idle;	// � ��������� ������ ������ �������

		Enable_Interrupt
		// ��������� ����������
		(GoToTask)();								// ��������� � ������
	}

	// set sleep mode
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);

	// sleep_mode() has a possible race condition
	sleep_enable()
	;
	Enable_Interrupt
	TWCR &= ~(1 << TWEN);

	sleep_cpu()
	;

	// Zzz...

	sleep_disable()
	;
	TWCR |= (1 << TWEN);
	// waking up...
	// disable external interrupt here, in case the external low pulse is too long
	// disable all interrupts
	Disable_Interrupt

}

inline void TimerService(void) {
	u08 index;

	for (index = 0; index != MainTimerQueueSize + 1; index++)// ����������� ������� ��������
			{
		if (MainTimer[index].GoToTask == Idle)
			continue;		// ���� ����� �������� - ������� ��������� ��������

		if (MainTimer[index].rtc_mode
				&& ++task_schedule_cnt == TASK_SCHEDULE_CHECK_CYCLE) {
			task_schedule_cnt = 0;
			unsigned char buf[3];
			unsigned char status = get_current_time(buf);
			if (status != TWI_SUCCESS || buf[2] != MainTimer[index].hour
					|| buf[1] != MainTimer[index].min) {
				continue;
			}
		} else if (MainTimer[index].delay != 1) {// ���� ������ �� ��������, �� ������� ��� ���. To Do: ��������� �� ������, ��� ����� !=1 ��� !=0. 
			MainTimer[index].delay--;// ��������� ����� � ������ ���� �� �����.
			continue;
		}

		SetTask(MainTimer[index].GoToTask);					// Put task to queue
		MainTimer[index].GoToTask = Idle;			// Set timer task to Idle
	}
}

// Convert normal decimal numbers to binary coded decimal
unsigned char decToBcd(unsigned char val) {
	return ((val / 10 * 16) + (val % 10));
}
// Convert binary coded decimal to normal decimal numbers
unsigned char bcdToDec(unsigned char val) {
	return ((val / 16 * 10) + (val % 16));
}

unsigned char get_current_time(unsigned char *buf) {
	// Read time
	unsigned char status;
	buf[0] = DS1307_ADR;
	buf[1] = 0;
	status = TWI_SendData(buf, 2);

	if (status == TWI_SUCCESS) {
		TWI_ReadData(buf, 8);
	}

	buf[0] = bcdToDec(buf[0]);
	buf[1] = bcdToDec(buf[1]);
	buf[2] = bcdToDec(buf[2]);

	//printf("%d %d %d \n", buf[2], buf[1], buf[0]);

	return status;
}
