#ifndef HAL_H
#define HAL_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avrlibtypes.h>
#include "avrlibdefs.h"
#include "avrlibtypes.h"
#include "avr/pgmspace.h"
#include <avr/wdt.h>
#include <stdio.h>
#include <avr/sleep.h>

//Clock Config
#define F_CPU 4000000L

//System Timer Config
#define Prescaler	  		64
#define	TimerDivider  		(F_CPU/Prescaler/1000)		// 1 mS

//USART Config
#define baudrate 9600L
#define bauddivider (F_CPU/(16*baudrate)-1)
#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

//PORT Defines
#define MOTOR_PORT PORTB
#define MOTOR_DDR DDRB
#define MOTOR_PIN 1

#define LED_PORT PORTB
#define LED_DDR DDRB
#define LED_PIN 6

#define RTC_INTERRUPT_PORT PORTD
#define RTC_INTERRUPT_DDR DDRD
#define RTC_INTERRUPT_PIN 2

extern void InitAll(void);
void USART0_write(unsigned char data);

#endif
