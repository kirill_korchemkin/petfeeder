#include <HAL.h>
#include <EERTOS.h>

#define FEEDER_DURATION_SEC 2
#define FEEDER_ON 0xff
#define FEEDER_OFF 0x00

unsigned char feed_time_array[3][3] = { {7, 20, 0 }, { 13, 0, 0 }, { 19, 0, 0 } };
unsigned char * next_feed_time = feed_time_array[2];

//RTOS Interrupt
ISR(RTOS_ISR) {
	TimerService();
}

// Task declaration ============================================================
void feeder_start(void);
void feeder_stop(void);
void set_next_feed_time(void);

// Main
int main(void) {
	InitAll();
	InitRTOS();
	RunRTOS();

	SetTimerTask(feeder_start, next_feed_time[0], next_feed_time[1],
			next_feed_time[2]);
	//SetTimerTaskDelay(feeder_start, 3);

	for (;;) {
		wdt_reset();
		TaskManager();
	}

	return 0;
}

//============================================================================
// Task definition
//============================================================================
void feeder_start(void) {
	OCR1A = FEEDER_ON;
	SetTimerTaskDelay(feeder_stop, FEEDER_DURATION_SEC);
}

void feeder_stop(void) {
	OCR1A = FEEDER_OFF;
	set_next_feed_time();
	SetTimerTask(feeder_start, next_feed_time[0], next_feed_time[1],
			next_feed_time[2]);
}

void set_next_feed_time(void) {
	if (next_feed_time == NULL) {
		next_feed_time = feed_time_array[0];
		return;
	}
	for (unsigned char i = 0; i < ARRLEN(feed_time_array); i++) {
		if (next_feed_time == feed_time_array[i]) {
			if (i == ARRLEN(feed_time_array) - 1) {
				next_feed_time = feed_time_array[0];
			} else {
				next_feed_time = feed_time_array[++i];
			}
			return;
		}
	}
}
