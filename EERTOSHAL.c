#include <EERTOSHAL.h>

//RTOS ������ ���������� �������
inline void RunRTOS(void) {
	// Reset all ISCxx bits
	MCUCR &= ~((1 << ISC11) | (1 << ISC10) | (1 << ISC01) | (1 << ISC00));
	//Enable external interrupt 
	GICR |= (1 << INT0);

	sei();
}
