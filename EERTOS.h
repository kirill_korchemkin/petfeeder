#ifndef EERTOS_H
#define EERTOS_H

#include <HAL.h>
#include <EERTOSHAL.h>
#include <twi.h>

extern void InitRTOS(void);
extern void Idle(void);

typedef void (*TPTR)(void);

extern void SetTask(TPTR TS);
extern void SetTimerTaskDelay(TPTR TS, u16 NewTime);
extern void SetTimerTask(TPTR TS, unsigned char hour, unsigned char min,
		unsigned char sec);

extern void TaskManager(void);
extern void TimerService(void);

unsigned char decToBcd(unsigned char val);
unsigned char bcdToDec(unsigned char val);

unsigned char get_current_time(unsigned char *buf);

//RTOS Errors ���� �� ������������.
#define TaskSetOk			 'A'
#define TaskQueueOverflow	 'B'
#define TimerUpdated		 'C'
#define TimerSetOk			 'D'
#define TimerOverflow		 'E'

#define TASK_SCHEDULE_CHECK_CYCLE 30

#endif
