#include <HAL.h>

FILE usart_str = FDEV_SETUP_STREAM(USART0_write, NULL, _FDEV_SETUP_WRITE);

inline void InitAll(void) {
	//InitUSART
	/* UBRRL = LO(bauddivider);
	 UBRRH = HI(bauddivider);
	 UCSRA = 0;
	 UCSRB = 1<<RXEN|1<<TXEN|0<<RXCIE|0<<TXCIE;
	 UCSRC = 1<<URSEL|1<<UCSZ0|1<<UCSZ1;
	 stdout = &usart_str;
	*/
	//InitPort
	//DDR 1 = output, 0 = input
	DDRB = 0b00000000;
	PORTB = 0b11111111;
	//DDRC = 0b00000000; PORTC = 0b00000000;
	DDRD = 0b00000000;
	PORTD = 0b11111111;
	//FastPWM Init
	MOTOR_DDR |= 1 << MOTOR_PIN;
	MOTOR_PORT &= ~(1 << MOTOR_PIN);
	OCR1A = 0x0;
	TCCR1A = (1 << COM1A1) | (1 << WGM12) | (1 << WGM10);
	TCCR1B = (1 << CS10);		 // Prescaler 1
	OCR1A = 0x00;
	TCCR0 = 0B11111000;
	TCCR2 = 0B11111000;

	// State LED
	//LED_DDR |= 1<<LED_PIN;

	// External interrupt port to input with pullup
	RTC_INTERRUPT_DDR &= ~(1 << RTC_INTERRUPT_PIN);
	RTC_INTERRUPT_PORT |= (1 << RTC_INTERRUPT_PIN);

	//disable ADC
	ADCSRA = 0;
}

void USART0_write(unsigned char data) {
	while (!( UCSRA & (1 << UDRE)))
		;
	UDR = data;
}

